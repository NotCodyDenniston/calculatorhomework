package com.example.calculator

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExposedDropdownMenuBoxScope
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue


@Composable
fun Calculator(){

    val firstText = remember { mutableStateOf("") }
    val secondText = remember { mutableStateOf("") }
    val result = remember { mutableStateOf("0") }
    val operator = remember { mutableStateOf("") }
    Column(verticalArrangement = Arrangement.SpaceAround, horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxSize()  )    {
        FirstTextField { newText: String ->
            firstText.value = newText
        }
    ExposedDropdownMenu(operator)
        SecondTextField{newText: String ->
            secondText.value = newText
        }
    fun calculate(operator:String){
        if(operator == "+"){
            result.value = (firstText.value.toInt() + secondText.value.toInt()).toString()
        }
        if(operator == "x"){
            result.value = (firstText.value.toInt() * secondText.value.toInt()).toString()
        }
        if(operator == "-"){
            result.value = (firstText.value.toInt() - secondText.value.toInt()).toString()
        }
        if(operator == "/"){
            result.value = (firstText.value.toInt() / secondText.value.toInt()).toString()
        }

    }
        Button(onClick = {calculate(operator.value)}){
            Text(text = "Calculate")
        }
        Text(text = "Result is ${result.value}")
    }
}

@Composable
fun FirstTextField(
    onTextChanged: (String) -> Unit
) {
    val text = remember { mutableStateOf("") }
    val pattern = remember { Regex("^\\d+\$") }
    Row {
        Text(text = "First Number")
        BasicTextField(
            value = text.value,
            onValueChange = {newText ->   if (newText.isEmpty() || newText.matches(pattern)){
                text.value = newText
                onTextChanged.invoke(newText)
            }},  modifier = Modifier.background(color = Color.LightGray),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)

    )
}
}

@Composable
fun ExposedDropdownMenu(operator:MutableState<String>){
var expanded = remember { mutableStateOf(false) }
    Column() {
    Text(text = "dropdown Menu", modifier = Modifier.clickable(onClick = {
        expanded.value = true
    }))
     DropdownMenu(expanded = expanded.value, onDismissRequest = {expanded.value = false}) {
         Text(text = "+", modifier = Modifier.clickable(onClick = { operator.value = "+" }))
         Text(text = "-", modifier = Modifier.clickable(onClick = { operator.value = "-" }))
         Text(text = "x", modifier = Modifier.clickable(onClick = { operator.value = "x" }))
         Text(text = "/", modifier = Modifier.clickable(onClick = { operator.value = "/" }))
     }
    }
}
@Composable
fun SecondTextField(
    onTextChanged: (String) -> Unit
) {
    val text = remember { mutableStateOf("") }
    val pattern = remember { Regex("^\\d+\$") }
    Row {
    Text(text = "Second Number")
        BasicTextField(
            value = text.value,
            onValueChange = { newText ->
                if (newText.isEmpty() || newText.matches(pattern)){
                    text.value = newText
                    onTextChanged.invoke(newText)
                }
            }, modifier = Modifier.background(color = Color.LightGray),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
    }
}

