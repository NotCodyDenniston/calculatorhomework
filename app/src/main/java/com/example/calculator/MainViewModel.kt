package com.example.calculator

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.geometry.Size
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    @Composable
    fun valueChange(newText: String) {
        val text = remember { mutableStateOf("") }
        val pattern = remember { Regex("^\\d+\$") }

        if (newText.isEmpty() || newText.matches(pattern)) {
            text.value = newText

        }
    }

}


